from pythonProject.tests.pages.basePage import BasePage
from pythonProject.tests.pages.mainPage import MainPage
from pythonProject.tests.pages.createForm import CreateForm
from pythonProject.tests.pages.loginPage import LoginPage

def test_create_task(browser):
    link = 'https://id.atlassian.com/login'
    page = BasePage(browser, link)
    page.open()
    page = LoginPage(browser,link)
    page.fillEmailField()
    page.clickButtonSubmit()
    page.fillPasswordField()
    page.clickLoginSubmitButton()
    page = MainPage(browser, link)
    page.chooseJira()
    page.clickButtonCreate()
    page = CreateForm(browser, link)
    page.fillSummaryField()
    page.clickButton()
