from selenium.webdriver.common.by import By


class MainPageLocators:
    buttonCreate = (By.XPATH, "//button[@id='createGlobalItemIconButton']")
    iconJira = (By.XPATH, "//button[@role='presentation']//div//div[@class='sc-ifAKCX kwIxZS']")


class CreateFormLocators:
    summary = (By.XPATH, "//input[@id='summary']")
    create = (By.XPATH, "// input[ @ id = 'create-issue-submit']")


class LoginPageLocators:
    emailField = (By.XPATH, "//input[@id='username']")
    submitButton = (By.XPATH, "//button[@id='login-submit']")
    passwordField = (By.XPATH, "//input[@id='password']")
    loginSubmitButton = (By.XPATH, "//button[@id='login-submit']")