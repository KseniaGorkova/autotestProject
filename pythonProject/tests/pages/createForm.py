from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from pythonProject.tests.pages.basePage import BasePage
from pythonProject.tests.pages.locators import CreateFormLocators


class CreateForm (BasePage):
    def fillSummaryField(self):
        summary_field = self.browser.find_element(*CreateFormLocators.summary)
        summary_field.send_keys("Баг")

    def clickButton(self):
        button_create = self.browser.find_element(*CreateFormLocators.create)
        button_create.click()


