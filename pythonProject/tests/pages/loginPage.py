from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from pythonProject.tests.pages.basePage import BasePage
from pythonProject.tests.pages.locators import LoginPageLocators


class LoginPage (BasePage):
    def fillEmailField(self):
        summary_field = self.browser.find_element(*LoginPageLocators.emailField)
        summary_field.send_keys("ksygorka@mail.ru")

    def clickButtonSubmit(self):
        button_create = self.browser.find_element(*LoginPageLocators.submitButton)
        button_create.click()

    def fillPasswordField(self):
        summary_field = self.browser.find_element(*LoginPageLocators.passwordField)
        summary_field.send_keys("anton10071998")

    def clickLoginSubmitButton(self):
        button_create = self.browser.find_element(*LoginPageLocators.loginSubmitButton)
        button_create.click()