from pythonProject.tests.pages.basePage import BasePage
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from pythonProject.tests.pages.locators import MainPageLocators


class MainPage(BasePage):
    def clickButtonCreate(self):
        button_create = self.browser.find_element(*MainPageLocators.buttonCreate)
        button_create.click()

    def chooseJira(self):
        jira = self.browser.find_element(*MainPageLocators.iconJira)
        jira.click()