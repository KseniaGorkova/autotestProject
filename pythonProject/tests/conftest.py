import pytest
from selenium import webdriver


@pytest.fixture(scope="function")
def browser():
    browser = webdriver.Chrome(executable_path=r'C:\Users\k.gorkova\Desktop\chromedriver\chromedriver.exe')
    #auth = requests.get('https://ksygorka.atlassian.net/login', auth=('ksygorka@mail.ru', 'anton10071998'))
    yield browser
    browser.quit()
